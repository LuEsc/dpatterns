package prototype;

public class Folder implements FolderClone {

    private String folderName;
    private String fileName;
    private String type;

    public Folder(String folderName, String fileName, String type) {
        this.folderName = folderName;
        this.fileName = fileName;
        this.type = type;
    }

    @Override
    public FolderClone createClone() {
       return new Folder(folderName, fileName, type);
    }

    @Override
    public String toString(){
        return "Folder cloned (type: " + type + " - name: " + folderName + " - file: " + fileName +")";
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
