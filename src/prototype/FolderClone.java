package prototype;

public interface FolderClone{
    FolderClone createClone();
}
