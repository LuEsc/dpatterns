import builder.Person;
import prototype.Folder;
import singleton.CountryService;

public class Main {
    public static void main(String[] args) {
        /**
         * Implementation of the singleton design pattern
         */
        CountryService listCountry = CountryService.getInstance();
        listCountry.getListCountry();

        /**
         * Implementation of the prototype design pattern
         */
        Folder newFolder = new Folder("Books",  "Angular in action", "Folder");
        Folder cloneFolder = (Folder) newFolder.createClone();
        Folder cloneFolderTwo = (Folder) newFolder.createClone();
        cloneFolderTwo.setFileName("The Gang of four");

        System.out.println(cloneFolder);
        System.out.println(cloneFolderTwo);

        /**
         * Implementation of the Builder design pattern
         */
        Person person = new Person.PersonBuilder()
                .name("Alvaro")
                .lastName("Pineda")
                .age(27)
                .address("Amsterdam")
                .nationality("Dutch")
                .build();
        System.out.println("Person created " + person);

    }
}