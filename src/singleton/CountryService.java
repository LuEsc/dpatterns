package singleton;

public class CountryService {
    private static CountryService instance;

    private CountryService() {}

    public static CountryService getInstance(){
        if (instance == null){
            return instance = new CountryService();
        }
        return instance;
    }

    public void getListCountry() {
        System.out.println("Get list of countries");
    }
}
