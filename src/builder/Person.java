package builder;

public class Person {

    private String name;
    private String lastName;
    private Integer age;
    private String address;
    private String nationality;

    private Person(PersonBuilder personBuilder){}

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public static class PersonBuilder {
        private String name;
        private String lastName;
        private Integer age;
        private String address;
        private String nationality;

        public PersonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PersonBuilder age(Integer age) {
            this.age = age;
            return this;
        }

        public PersonBuilder address(String address) {
            this.address = address;
            return this;
        }

        public PersonBuilder nationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public Person build() { return  new Person(this); }
    }
}
